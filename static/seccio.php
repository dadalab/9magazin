    <!doctype html>
    <html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>9MAGAZIN</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->

        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css">
        <script src="js/vendor/modernizr-2.8.3.min.js"></script>

    </head>
    <body>
    <!--[if lt IE 8]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
                
    <div id="navegation-site" class="overlay">
        <div class="container" style="position: relative;">             

                <!-- hamburger -->
                <button class="hamburger hamburger--squeeze js-hamburger is-active" type="button">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
                </button>

                <!-- logo -->
                <a class="navbar-brand" href="http://localhost:81/el9nou/wp"><img src="http://localhost:81/el9nou/wp/wp-content/themes/el9nou/img/el9nou.cat-white.png"></a>

                <!-- search -->
                <div class="search"><i class="icon-lupa"></i></div>


                      <hr style="border-color: #FFF; margin-top: 32px;">
                      <div class="row">
                        <div class="col-md-2">
                            <ul class="menu">
                                <li>EL DIARI</li>
                                <li><a href="#">Política</a></li>
                                <li><a href="#">Societat</a></li>
                                <li><a href="#">Economia</a></li>
                                <li><a href="#">Futbol</a></li>
                                <li><a href="#">Hoquei patins</a></li>
                                <li><a href="#">Motor</a></li>
                                <li><a href="#">Arts escèniques</a></li>
                                <li><a href="#">Cultura popular</a></li>
                                <li><a href="#">Llibres</a></li>
                                <li><a href="#">Calaix</a></li>
                            </ul>
                        </div>
                        <div class="col-md-2">
                            <ul class="menu">
                                <li>SERVEIS</li>
                                <li><a href="#">El trànsit</a></li>
                                <li><a href="#">El temps</a></li>            
                                <li><a href="#">Cartellera</a></li>            
                                <li><a href="#">Horaris RENFE</a></li>            
                                <li><a href="#">Horari BUS</a></li>            
                                <li><a href="#">Farmàcies</a></li>            
                                <li><a href="#">Adreces d’interès</a></li>            
                                <li><a href="#">Emergències</a></li>            
                            </ul>
                        </div>
                        <div class="col-md-2">
                            <ul class="menu">
                                <li>AGENDA</li>
                                <li><a href="#">Cultural</a></li>
                                <li><a href="#">Esportiva</a></li>
                                <li><a href="#">Infantil</a></li>
                                <li><a href="#">Oci</a></li>
                            </ul>
                        </div>
                        <div class="col-md-2">
                            <ul class="menu">
                                <li>SORTIM (9MAGAZÍN)</li>
                                <li><a href="#">Oci</a></li>
                                <li><a href="#">Cultura</a></li>
                                <li><a href="#">Gastronomia</a></li>
                                <li><a href="#">Viatges</a></li>
                                <li><a href="#">Amb nens</a></li>
                            </ul>
                        </div>
                        <div class="col-md-2">
                            <ul class="menu">
                                <li>FEM SALUT (9MAGAZÍN)</li>
                                <li><a href="#">Salut</a></li>
                                <li><a href="#">Bellesa</a></li>
                                <li><a href="#">Esport</a></li>            
                                <li><a href="#">Alimentació</a></li>            
                            </ul>
                        </div>
                        <div class="col-md-2">
                            <ul class="menu">
                                <li>VIDA (9MAGAZÍN)</li>
                                <li><a href="#">Hort i jardí</a></li>               
                                <li><a href="#">Hobbies</a></li>               
                                <li><a href="#">Sostenibilitat</a></li>               
                                <li><a href="#">Producte de proximitat</a></li>               
                            </ul>
                        </div>
                    </div>
                    <div class="clear"></div>
                    <hr style="border-color: #FFF;">

        </div>
    </div>
    <!-- MENU OVERLAY -->
    <div class="topbar-wrapp">
        <div class="col-md-4 col-sm-5 hidden-xs text-left">
            <ul class="corporative">
                <li><a href="http://localhost:81/el9nou/wp">El 9 NOU</a></li>
                <li><a href="http://localhost:81/el9nou/wp/9tv">El 9 TV</a></li>
                <li><a href="http://localhost:81/el9nou/wp/9fm">El 9 FM</a></li>
                <li><a href="#">9CLICS</a></li>
            </ul>
        </div>
        <div class="col-md-4 col-sm-2 text-center"><div class="data">19 juliol 2016</div></div>
        <div class="col-md-4 col-sm-5 hidden-xs text-right">
            <ul class="user">
                <li><a href="#">Subscriu-te</a></li>
                <li><a href="#">Inicia sessió</a></li>
            </ul>
        </div>
    </div>

    <!-- HEADER -->
    <header class="header" style="background: url(img/magazin-home.jpg) no-repeat center center fixed;">            
        <div class="header-wrapp container">
                <!-- Suplement -->
                <h1 class="suplement">Suplement</h1>
                <!-- menu toggle -->
                <button class="hamburger hamburger--squeeze js-hamburger" type="button">
                      <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
                </button>
                <!-- navbar-brand -->
                <a class="navbar-brand" href="#">
                    <img src="img/9magazin.svg">
                </a>
                <!-- search -->
                <div class="search"><i class="icon-lupa"></i></div>
                
                <!-- tags -->
                <a href="#"><div class="index-tags"><h2>ÍNDEX</h2></div></a>

                <!--
                <ul class="tags">
                    <li><a href="#">CINEMA</a></li>
                    <li><a href="#">RECOMANACIONS CULTURALS</a></li>
                    <li><a href="#">OCI</a></li>
                    <li><a href="#">ENTENDRE L’ECONOMIA</a></li>
                    <li><a href="#">LLIBRES</a></li>
                    <li><a href="#">NOTÍCIES CURIOSES</a></li>
                    <li><a href="#">VIURE PAUSAT</a></li>
                    <li><a href="#">MÚSICA</a></li>
                    <li><a href="#">ENTENDRE L’ECONOMIA</a></li>
                    <li><a href="#">LLIBRES</a></li>
                    <li><a href="#">NOTÍCIES CURIOSES</a></li>
                    <li><a href="#">VIURE PAUSAT</a></li>
                    <li><a href="#">MÚSICA</a></li>
                    <li><a href="#">+++</a></li>
                </ul>
                -->               

        </div><!-- / header-wrapp -->           
    </header>

        
        <!-- navigation -->
        <nav id="navigation">
            <div class="container">
                <!-- hamburger -->
                <button class="hamburger hamburger--squeeze js-hamburger" type="button">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
                </button>
                <a class="navbar-brand" href="#">
                    <img src="img/9magazin.svg">
                </a>
                <!-- search -->
                <div class="search"><i class="icon-lupa"></i></div>
            </div>
        </nav>
        <!-- / navigation -->

        <section id="seccio">
            <div class="container">
                <div class="list-posts">

                    <!-- post destacat -->
                    <div class="row">
                        <div class="post col-md-12">
                            <h1 class="seccio">Cinema</h1>
                            <a href="fitxa.php">
                                <figure>
                                    <img src="img/magazine-post.jpg">
                                    <div class="sponsored"><span>PATROCINAT</span> <img src="img/sponsor.jpg"></div>
                                </figure>
                            </a>
                            <div class="row">
                                <div class="titol col-md-6">
                                    <a href="fitxa.php"><h1>SIETE DIOSAS</h1></a>                                   
                                </div>
                                <div class="col-md-6">
                                    <p>En la idílica playa de Goa, Frieda, una fotógrafa de éxito, reúne a sus mejores amigas 
                                        en la víspera de su boda. En la idílica playa de Goa, Frieda, una fotógrafa de éxito, reúne a sus mejores amigas en la víspera de su boda.</p>
                                    </div>
                                    <div class="category">ESTRENES DE LA SETMANA</div>
                                    <div class="compartir"><a href="#"><i class="icon-share"></i></a></div>
                                </div>
                                <hr><!-- line -->
                            </div><!-- / post -->
                        </div>
                        <!-- / post destacat -->
                        
                        <div class="row grid">                       

                            <div class="post col-md-6 grid-sizer grid-item">
                                <h1>Oci</h1>
                                <a href="fitxa.php">
                                    <figure>
                                        <img src="img/magazine-post-1.jpg">
                                        <div class="sponsored"><span>PATROCINAT</span> <img src="img/sponsor.jpg"></div>
                                    </figure>
                                </a>
                                <div class="row">
                                    <div class="titol col-md-12">
                                        <a href="fitxa.php"><h1>SIETE DIOSAS</h1></a>
                                    </div>
                                    <div class="titol col-md-12">
                                        <p>En la idílica playa de Goa, Frieda, una fotógrafa de éxito, reúne a sus mejores amigas 
                                            en la víspera de su boda. En la idílica playa de Goa, Frieda, una fotógrafa de éxito, reúne a sus mejores amigas en la víspera de su boda.</p>
                                        </div>
                                        <div class="category">ESTRENES DE LA SETMANA</div>
                                        <div class="compartir"><a href="#"><i class="icon-share"></i></a></div>
                                    </div>
                                    <hr><!-- line -->
                                </div><!-- / post -->

                                <div class="post col-md-6 grid-item">
                                    <h1>Cultura</h1>
                                    <a href="fitxa.php">
                                        <figure>
                                            <img src="img/magazine-post-2.jpg">
                                            <div class="sponsored"><span>PATROCINAT</span> <img src="img/sponsor.jpg"></div>
                                        </figure>
                                    </a>
                                    <div class="row">
                                        <div class="titol col-md-12">
                                            <a href="fitxa.php"><h1>SIETE DIOSAS</h1></a>
                                        </div>
                                        <div class="col-md-12">
                                            <p>En la idílica playa de Goa, Frieda, una fotógrafa de éxito, reúne a sus mejores amigas 
                                                en la víspera de su boda. En la idílica playa de Goa, Frieda, una fotógrafa de éxito, reúne a sus mejores amigas en la víspera de su boda.</p>
                                            </div>
                                            <div class="category">ESTRENES DE LA SETMANA</div>
                                            <div class="compartir"><a href="#"><i class="icon-share"></i></a></div>
                                        </div>
                                        <hr><!-- line -->
                                    </div><!-- / post -->

                                    <div class="post col-md-6 grid-item">
                                        <h1>Tendències</h1>
                                        <a href="fitxa.php">
                                            <figure>
                                                <img src="img/magazine-post-3.jpg">
                                                <div class="sponsored"><span>PATROCINAT</span> <img src="img/sponsor.jpg"></div>
                                            </figure>
                                        </a>
                                        <div class="row">
                                            <div class="titol col-md-12">
                                                <a href="fitxa.php"><h1>SIETE DIOSAS</h1></a>
                                            </div>
                                            <div class="col-md-12">
                                                <p>En la idílica playa de Goa, Frieda, una fotógrafa de éxito, reúne a sus mejores amigas 
                                                    en la víspera de su boda. En la idílica playa de Goa, Frieda, una fotógrafa de éxito, reúne a sus mejores amigas en la víspera de su boda.</p>
                                                </div>
                                                <div class="category">ESTRENES DE LA SETMANA</div>
                                                <div class="compartir"><a href="#"><i class="icon-share"></i></a></div>
                                            </div>
                                            <hr><!-- line -->
                                        </div><!-- / post -->

                                        <div class="post col-md-6 grid-item">
                                           <h1>Llibres</h1>
                                           <a href="fitxa.php">
                                               <figure>
                                                    <img src="img/magazine-post-4.jpg">
                                                    <div class="sponsored"><span>PATROCINAT</span> <img src="img/sponsor.jpg"></div>
                                                </figure>
                                            </a>
                                        <div class="row">
                                            <div class="titol col-md-12">
                                                <a href="fitxa.php"><h1>SIETE DIOSAS</h1></a>
                                            </div>
                                            <div class="col-md-12">
                                                <p>En la idílica playa de Goa, Frieda, una fotógrafa de éxito, reúne a sus mejores amigas 
                                                    en la víspera de su boda. En la idílica playa de Goa, Frieda, una fotógrafa de éxito, reúne a sus mejores amigas en la víspera de su boda.</p>
                                                </div>
                                                <div class="category">ESTRENES DE LA SETMANA</div>
                                                <div class="compartir"><a href="#"><i class="icon-share"></i></a></div>
                                            </div>
                                            <hr><!-- line -->
                                        </div><!-- / post -->

                                        <div class="post col-md-6 grid-item">
                                            <h1>Música</h1>
                                            <a href="fitxa.php">
                                                <figure>
                                                    <img src="img/magazine-post-5.jpg">
                                                    <div class="sponsored"><span>PATROCINAT</span> <img src="img/sponsor.jpg"></div>
                                                </figure>
                                            </a>
                                            <div class="row">
                                                <div class="titol col-md-12">
                                                    <a href="fitxa.php"><h1>SIETE DIOSAS</h1></a>
                                                </div>
                                                <div class="col-md-12">
                                                    <p>En la idílica playa de Goa, Frieda, una fotógrafa de éxito, reúne a sus mejores amigas 
                                                        en la víspera de su boda. En la idílica playa de Goa, Frieda, una fotógrafa de éxito, reúne a sus mejores amigas en la víspera de su boda.</p>
                                                    </div>
                                                    <div class="category">ESTRENES DE LA SETMANA</div>
                                                    <div class="compartir"><a href="#"><i class="icon-share"></i></a></div>
                                                </div>
                                                <hr><!-- line -->
                                            </div><!-- / post -->

                                            <div class="post col-md-6 grid-item">
                                               <h1>Vida</h1>
                                               <a href="fitxa.php">
                                                   <figure>
                                                        <img src="img/magazine-post-6.jpg">
                                                        <div class="sponsored"><span>PATROCINAT</span> <img src="img/sponsor.jpg"></div>
                                                    </figure>
                                                </a>
                                            <div class="row">
                                                <div class="titol col-md-12">
                                                    <a href="fitxa.php"><h1>SIETE DIOSAS</h1></a>
                                                </div>
                                                <div class="col-md-12">
                                                    <p>En la idílica playa de Goa, Frieda, una fotógrafa de éxito, reúne a sus mejores amigas 
                                                        en la víspera de su boda. En la idílica playa de Goa, Frieda, una fotógrafa de éxito, reúne a sus mejores amigas en la víspera de su boda.</p>
                                                    </div>
                                                    <div class="category">ESTRENES DE LA SETMANA</div>
                                                    <div class="compartir"><a href="#"><i class="icon-share"></i></a></div>
                                                </div>
                                                <hr><!-- line -->
                                            </div><!-- / post -->

                                        </div><!-- / row -->
                                        
                                    </div>
                                </div>
                            </section>

                            <footer>
                               <!-- sitemap -->
                               <section class="sitemap">
                                   <div class="container">
                                       <div class="col-md-2">
                                        <ul class="footer-list">
                                            <li>EL DIARI</li>
                                            <li>Política i societat</li>
                                            <li>Economia</li>
                                            <li>Cultura</li>
                                            <li>Esports</li>
                                        </ul>
                                    </div>
                                    <div class="col-md-2">
                                       <ul class="footer-list">
                                        <li>SERVEIS</li>
                                        <li>El trànsit</li>
                                        <li>El temps</li>
                                        <li>Cartellera</li>
                                        <li>Horaris RENFE</li>
                                        <li>Horari BUS</li>
                                        <li>Farmàcies</li>
                                        <li>Adreces d’interès</li>
                                        <li>Emergències</li>
                                    </ul>
                                </div>
                                <div class="col-md-2">
                                    <ul class="footer-list">
                                        <li>AGENDA</li>
                                        <li>Cultural</li>
                                        <li>Esportiva</li>
                                        <li>Infantil</li>
                                        <li>Oci</li>                            
                                    </ul>
                                </div>
                                <div class="col-md-2">
                                    <ul class="footer-list">
                                        <li>SORTIM (+més9)</li>
                                        <li>Oci</li>
                                        <li>Cultura</li>
                                        <li>Gastronomia</li>
                                        <li>Viatges</li>                            
                                        <li>Amb nens</li>                            
                                    </ul>
                                </div>
                                <div class="col-md-2">
                                    <ul class="footer-list">
                                        <li> FEM SALUT (+més9)</li>
                                        <li>Salut</li>
                                        <li>Bellesa</li>
                                        <li>Esport</li>
                                        <li>Alimentació</li>                                                                           
                                    </ul>                        
                                </div>
                                <div class="col-md-2">
                                    <ul class="footer-list">
                                        <li>VIDA (+més9)</li>
                                        <li>Hort i jardí</li>
                                        <li>Hobbies</li>
                                        <li>Sostenibilitat</li>
                                        <li>Producte de proximitat</li>                                                                           
                                    </ul>
                                </div>
                            </div>
                        </section>
                        <!-- sitemap -->

                        <section class="ads">
                           <div class="container">
                               <div class="col-md-6">
                                <div class="newsletter-cont">
                                   <h1>BUTLLETÍ DE NOTÍCIES</h1>
                                   <input type="text" name="email" placeholder="Correu electrònic">
                                   <button class="btn-newsletter">Subscriure'm</button>
                               </div>

                               <div class="social-cont">
                                   <h1>XARXES SOCIALS</h1>
                                   <ul class="footer-socials">
                                       <li><a href="#"><i class="icon-facebook-circled"></i></a></li>
                                       <li><a href="#"><i class="icon-twitter-circled"></i></a></li>
                                       <li><a href="#"><i class="icon-instagram-circled"></i></a></li>
                                   </ul>
                               </div>
                           </div>
                           <div class="col-md-6">
                               <figure><img src="http://localhost:81/el9nou/wp/wp-content/themes/el9nou/img/banner-footer.jpg"></figure>
                           </div>
                       </div>
                   </section>

                   <section class="group">
                       <div class="container">
                           <div class="col-md-6">
                               <h1>EL GRUP</h1>
                               <ul class="logos-corp">
                                   <li style="width: 66px;"><img src="http://localhost:81/el9nou/wp/wp-content/themes/el9nou/img/el-9fm.png"></li>
                                   <li style="width: 100px;"><img src="http://localhost:81/el9nou/wp/wp-content/themes/el9nou/img/el-9nou.png"></li>
                                   <li style="width: 70px;"><img src="http://localhost:81/el9nou/wp/wp-content/themes/el9nou/img/el-9tv.png"></li>
                               </ul>
                           </div>
                           <div class="col-md-3">
                               <h1>AMB LA COL·LABORACIÓ</h1>
                               <figure><img class="gencat" src="http://localhost:81/el9nou/wp/wp-content/themes/el9nou/img/gencat.jpg"></figure>
                           </div>
                           <div class="col-md-3">
                            <h1>DADES LEGALS</h1>
                            <ul class="legal">
                                <li><a href="#">Política privacitat</a></li>
                                <li><a href="#">Política de cookies</a></li>
                                <li><a href="#">Avís legal</a></li>
                            </ul>
                        </div>
                    </div>
                </section>

                <div class="socket">
                   <div class="container">
                       <div class="col-md-8 text-left">PREMSA D´OSONA, SA  |  CTL  |  Tarifes subscripcions  |  Publicacions oficials  |  Contacteu-nos </div>
                       <div class="col-md-4 text-right">© Creative Commons - 2016</div>
                   </div>
               </div>
           </footer>
            
           <!-- Share Overlay -->
           <div class="overlay-share">              
               <div class="overlayInner">
                   <button class="hamburger hamburger--squeeze js-hamburger is-active" type="button">
                        <span class="hamburger-box">
                            <span class="hamburger-inner"></span>
                        </span>
                   </button>
                   <ul>
                       <li>Compartir</li>
                       <li><a href="#">Facebook</a></li>
                       <li><a href="#">Twitter</a></li>
                       <li><a href="#">Google +</a></li>
                       <li><a href="#">LinkedIn</a></li>                       
                   </ul>
               </div>
           </div>
           <!-- / Share Overlay -->

           <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
           <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.12.0.min.js"><\/script>')</script>
           <script src="https://npmcdn.com/isotope-layout@3.0/dist/isotope.pkgd.min.js"></script>

           <script src="js/plugins.js"></script>
           <script src="js/main.js"></script>

           <script type="text/javascript">
             $('.grid').isotope({
                  // set itemSelector so .grid-sizer is not used in layout
                  itemSelector: '.grid-item',
                  percentPosition: true,
                  masonry: {
                    // use element for option
                    columnWidth: '.grid-sizer'
                }
            })
        </script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
                function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='https://www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X','auto');ga('send','pageview');
        </script>
    </body>
    </html>
