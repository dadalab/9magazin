
$( document ).ready(function() {

    /////////////////////////////////////
    // HAMBURGER MENU 
    var $hamburger = $("header .hamburger, #navegation-site .hamburger, #navigation .hamburger");   
    var $body = $("body");
    var $overlay = $("#navegation-site");
    var $share = $(".compartir, .overlay-share .hamburger");   
    var $overlayShare = $(".overlay-share");

  	$hamburger.on("click", function(e) {
      e.preventDefault();
	   	$body.toggleClass("in");
	    $overlay.toggleClass("in");
	 });  

    $share.on("click", function(e) {
      e.preventDefault();
        $body.toggleClass("in");
        $overlayShare.toggleClass("in");
     }); 

    /////////////////////////////////////
    // SECTION ID
    $sectionId = $('section').attr('id');
    $('body').addClass($sectionId);
    $('header').addClass($sectionId);
    $('footer').addClass($sectionId);

});

 
///////////////////////////////////////////////
// ONLOAD RESIZE
$(window).on("load resize",function(){

    // center overlay 
    var windowh = $(window).height();
    var windoww = $(window).width();
    var overlayh = $('.overlayInner').outerHeight();
    var overlayw = $('.overlayInner').width();

    $('.overlayInner').css('top', ((windowh - overlayh) / 2) + 'px');  
    $('.overlayInner').css('left', ((windoww - overlayw) / 2) + 'px');

    console.log(overlayw);

    // background-image FULLWIDTH div
    var ww = $(window).width();
    var wh1 = $(window).height();
    $('header.home').css("height", wh1);

    // center HEADER-WRAPP 
    var wh = $(window).height();
    var ww = $(window).width();
    var sh = $('.header-wrapp').outerHeight();  
    var sw = $('.header-wrapp').width();  
    
    $('.header-wrapp').css('top', ((wh - sh) / 2) + 'px');
    $('.header-wrapp').css('left', ((ww - sw) / 2) + 'px');
    
});


///////////////////////////////////////////////
// HEADER MENU SCROLL OPACITY + BG ANIMATION
$(window).scroll(function() {    
    var x = $(this).scrollTop();    
    $('header.header').css('background-position','center '+parseInt(-x/5)+'px');
    $('.header-wrapp').css({ 'opacity' : (1 - x/305) }); 
    $('.header.header').css({ 'opacity' : (1 - x/1005) }); 
    $('.topbar-wrapp').css({ 'opacity' : (1 - x/1005) }); 
    
});

///////////////////////////////////////////////
// Scroll Down HEADER ANIMATION 
var lastScrollTop = 0;

$(window).on("load resize",function(){

    var st = $(this).scrollTop();
    var top = $("#home, #seccio").offset().top;
    $('#navigation').removeClass('out');

   if (st > lastScrollTop){
       // downscroll code
       if (st > top - 40){
            $('#navigation').addClass('animate-width');
        } 
   } else {
      // upscroll code
       if (st < top - 40){
            $('#navigation').addClass('out');
            setTimeout(function(){
                $('#navigation').removeClass('animate-width');
            }, 100);
       } 
   }
   lastScrollTop = st;
});

////////////////////////////////////////////////
// Scroll Down HEADER ANIMATION 
$(window).scroll(function(event){

   var st = $(this).scrollTop();
   var top = $("#home, #seccio").offset().top;
   $('#navigation').removeClass('out');

   if (st > lastScrollTop){
       // downscroll code
       if (st > top - 40){
            $('#navigation').addClass('animate-width');
        } 
   } else {
      // upscroll code
       if (st < top - 40){
            $('#navigation').addClass('out');
            setTimeout(function(){
                $('#navigation').removeClass('animate-width');
            }, 100);
       } 
   }
   lastScrollTop = st;
});


/*
$(function(){
    $(window).scroll(function(){
        var top = $("#home").offset().top;
        
        console.log($(this).scrollTop());
        console.log(top);

        if ($(this).scrollTop() > top - 40){
            $('#navigation').addClass('animate-width');
        } else {           
            //$('#navigation').removeClass('animate-width').addClass('out');           
        }   
       
    });
});
*/


/*
$(window).scroll(function(){       
    if ($(window).scrollTop() > 0) {
        $('#navigation').stop().animate({width: fullwidth}, 200);
    } else {
        $('#navigation').stop().animate({width: smallwidth}, 200);
    }
});
*/