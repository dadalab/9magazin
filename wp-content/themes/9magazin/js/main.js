
$( document ).ready(function() {


    // section#home

     /* ICON DOWN CLICK */
    $(".down-arrow").click(function(e) {        
        e.preventDefault();
        var graella = $("section#home").offset().top;
        $('html, body').animate({scrollTop: graella }, 1600, 'easeInOutExpo');

        //console.log("!adsdsad");
    });


    /////////////////////////////////////
    // HAMBURGER MENU 
    var $hamburger = $("header .hamburger, #navegation-site .hamburger, #navigation .hamburger");   
    var $body = $("body");
    var $overlay = $("#navegation-site");
    var $share = $(".compartir, .overlay-share .hamburger");   
    var $overlayShare = $(".overlay-share");

  	$hamburger.on("click", function(e) {
      e.preventDefault();
	   	$body.toggleClass("in");
	    $overlay.toggleClass("in");
	 });  

    $share.on("click", function(e) {
        var shareSites = [
            'share-facebook', 'share-twitter', 'share-google', 'share-link'
        ];
        var url = $(this).attr('data-url');
        var title = $(this).attr('data-title');
        for (var i = 0; i < shareSites.length; ++i)
        {
            $('#' + shareSites[i]).attr('href', $('#' + shareSites[i]).attr('data-share-url') + url);
        }
        $('#share-whatsapp').attr('href', 'whatsapp://send?text=' + encodeURIComponent(url));

        e.preventDefault();
        $body.toggleClass("in");
        $overlayShare.toggleClass("in");
     }); 

    /////////////////////////////////////
    // SECTION ID
    $sectionId = $('section').attr('id');
    $('body').addClass($sectionId);
    $('header').addClass($sectionId);
    $('footer').addClass($sectionId);


     var windowWidth = $(window).width();

    //////////////////////////////////////////
    // MENU DESPLEGALBE MOBILE
    /////////////////////////////////////////

    if(windowWidth < 768) { 
    
         $("h2.options").on("click", function(e) {   
           e.preventDefault();
          //  console.log("FUMMMMMM!");

           if ( $(this).hasClass( "minus" ) ) {          
                $(this).removeClass("minus");
                $(this).next("ul.menu").removeClass("down");  
           } else {
              $("h2.options").removeClass("minus");
              $("ul.menu").removeClass("down");
              $(this).addClass("minus");
              $(this).next("ul.menu").addClass("down");   
           }
          //  $("ul.menu").slideUp(300);       
          //  $(this).next("ul.menu").slideToggle(300);   
        //  $(this).toggleClass("minus");           
        }); 

      $("h2.title").click(function(e){
        e.preventDefault();
        $(".toggle").slideToggle(250);
      });

      $(".grup-corporatiu h2").click(function(e){
          e.preventDefault();
          $(".logos-corporative").slideToggle(250);
      });

    }
    ////


});

 
 
///////////////////////////////////////////////
// ONLOAD RESIZE
$(window).on("load resize",function(){

    // center overlay 
    var windowh = $(window).height();
    var windoww = $(window).width();
    var overlayh = $('.overlayInner').outerHeight();
    var overlayw = $('.overlayInner').width();

    $('.overlayInner').css('top', ((windowh - overlayh) / 2) + 'px');  
    $('.overlayInner').css('left', ((windoww - overlayw) / 2) + 'px');

    console.log(overlayw);

    // background-image FULLWIDTH div
    var ww = $(window).width();
    var wh1 = $(window).height();
    $('header.home').css("height", wh1);

    // center HEADER-WRAPP 
    var wh = $(window).height();
    var ww = $(window).width();
    var sh = $('header.home .header-wrapp').outerHeight();  
    var sw = $('header.home .header-wrapp').width();  
    
    $('header.home .header-wrapp').css('top', ((wh - sh) / 2) + 'px');
    $('header.home .header-wrapp').css('left', ((ww - sw) / 2 - 15) + 'px');
    $('header.home .header-wrapp').css('right', ((ww - sw) / 2) + 'px');

    //$('loading')
    
});

$(document).ready(function() {    

    setTimeout(function() {
            $('.generalLoader').fadeOut();
            $('body').removeClass('notLoaded');
            $('body').addClass('loaded');
        }, 100)   // Add delay for lazy load simulation   

     $(function () {
       
    });
    
    
});



///////////////////////////////////////////////
// HEADER MENU SCROLL OPACITY + BG ANIMATION
$(window).scroll(function() {    
    var x = $(this).scrollTop();    
    $('header.header').css('background-position','center '+parseInt(-x/5)+'px');
    $('.header-wrapp').css({ 'opacity' : (1 - x/305) }); 
    $('.header.header').css({ 'opacity' : (1 - x/1005) }); 
    $('.topbar-wrapp').css({ 'opacity' : (1 - x/1005) }); 
    $('.down').css({ 'opacity' : (1 - x/305) }); 
    
});

///////////////////////////////////////////////
// Scroll Down HEADER ANIMATION 
var lastScrollTop = 0;

$(window).on("load resize",function(){

    var st = $(this).scrollTop();
    var top = $("#home, #seccio").offset().top;
    $('#navigation').removeClass('out');

   if (st > lastScrollTop){
       // downscroll code
       if (st > top - 40){
            $('#navigation').addClass('animate-width');
        } 
   } else {
      // upscroll code
       if (st < top - 40){
            $('#navigation').addClass('out');
            setTimeout(function(){
                $('#navigation').removeClass('animate-width');
            }, 100);
       } 
   }
   lastScrollTop = st;
});

////////////////////////////////////////////////
// Scroll Down HEADER ANIMATION 
$(window).scroll(function(event){

   var st = $(this).scrollTop();
   var top = $("#home, #seccio").offset().top;
   $('#navigation').removeClass('out');

   if (st > lastScrollTop){
       // downscroll code
       if (st > top - 40){
            $('#navigation').addClass('animate-width');
        } 
   } else {
      // upscroll code
       if (st < top - 40){
            $('#navigation').addClass('out');
            setTimeout(function(){
                $('#navigation').removeClass('animate-width');
            }, 100);
       } 
   }
   lastScrollTop = st;
});

Vue.component('likes-counter', {
    props: ['likes', 'id', 'loading', 'activeColor', 'recalculate'],
    template: '<a href="#" v-bind:id="id" @click="increment"><i v-if="loading" class="fa fa-spinner fa-spin fa-fw"></i><span v-if="!loading">{{ likes }}</span> <i v-bind:style="{ color: activeColor }" class="fa fa-heart" aria-hidden="true"></i></a>',
    computed: {
        activeColor: function () {
            this.recalculate = 1;
            console.log(this.id);
            return this.isFavorited() ? '#c4003e' : '#d3d3d8';
        }
    },
    methods: {
        isFavorited: function() {
            var favorites = JSON.parse(localStorage.getItem('favorites')) || [];
            return favorites.indexOf(this.id) != -1;
        },
        increment: function (e) {
            e.preventDefault();
            if (! this.loading) {
                this.loading = true;
                var data = {
                    'action': this.isFavorited() ? 'decrease_count' : 'increase_count',
                    'post_id': this.id 
                };
                var favorites = JSON.parse(localStorage.getItem('favorites')) || [];
                if (this.isFavorited()) {
                    var elementToDelete = favorites.indexOf(this.id);
                    favorites.splice(elementToDelete, 1);
                    $('a#' + this.id + ' i').css('color' , '#d3d3d8');
                } else {
                    favorites.push(this.id); 
                    $('a#' + this.id + ' i').css('color' , '#c4003e');
                }
                this.recalculate = 0;
                localStorage.setItem("favorites", JSON.stringify(favorites));
                var that = this;
                jQuery.post('/wp-admin/admin-ajax.php', data, function(response) {
                    that.likes = parseInt(response);
                    that.loading = false;
                });
            }
        }
    },
});

new Vue({
  el: '#home'
});

new Vue({
  el: '#seccio'
});

/***** User Logged In ****/
var LoggedIn = {
  props: ['edicio'],

  data: function () {
    return {
      user: '',
      loading: true,
  }
},

    ready: function() {
        $.ajaxSetup({
            xhrFields: {
                withCredentials: true
            }
        });

        var that = this;
        $.ajax({ url: "https://9club.el9nou.cat/api/usuari", }).done(function(data) {
            that.user = data.user;
            user = data.user;
            that.loading = false;
        });
    }
}

new Vue({
  el: '#user-logged-in',

  components: {
    'user-logged-in': LoggedIn
}
});