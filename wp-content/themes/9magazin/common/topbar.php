<div class="topbar-wrapp">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-4 hidden-xs text-left">
                <ul class="corporative">
                    <li><a href="<?php url_site('el9nou') ?>">EL 9 NOU</li>
	                <li><a href="<?php url_site('9club') ?>">el9club</a></li>
                    <li class="hidden-sm hidden-xs"><a href="<?php url_site('el9tv') ?>">EL 9 TV</a></li>
                    <li class="hidden-sm hidden-xs"><a href="<?php url_site('el9fm') ?>">EL 9 FM</a></li>               
                </ul>
            </div>
            <div class="col-md-4 col-sm-4 hidden-xs text-center"><div class="data"><?php echo date_i18n('j F Y', time()); ?></div></div>
            <div class="col-md-4 col-sm-4 text-right">
            <div id="user-logged-in">
                        <user-logged-in inline-template v-cloak>
                            <ul class="user" v-if=" ! loading ">
                                <li v-if="user"><a href="http://9club.el9nou.cat/usuari">Hola {{ user.nom }} {{ user.cognoms }}!</a></li>
                                <li v-if="! user"><a href="http://9club.el9nou.cat/osona-ripolles/registre">Registra't</a></li>
                                <li v-if="! user"><a href="http://9club.el9nou.cat/osona-ripolles/entra?redirect=<?php the_permalink() ?>">Inicia sessió</a></li>
                            </ul>
                        </user-logged-in>
                    </div>
            </div>
        </div>
    </div>
</div>