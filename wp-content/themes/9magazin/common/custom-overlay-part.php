<hr class="linia">
    <div class="row this-options">
        <div class="col-md-3 col-sm-3 hidden-xs">
            <a class="brand magazin" href="<?php echo site_url() ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/9magazin.svg"></a>
        </div>
        <div class="col-md-9 col-sm-9">
            <ul class="menu-large">
                <li>9MAGAZÍN</li><?php
                    $recent_posts = wp_get_recent_posts(['numberposts' => 4, 'post_status' => 'publish']);
                    foreach ($recent_posts as $recent) {
                        echo '<li><a href="' . get_permalink($recent["ID"]) . '">' .   $recent["post_title"].'</a> </li> ';
                    }
                    wp_reset_query(); ?>
            </ul>
    </div>
    
    <div class="row this-options">
        <div class="col-md-12 col-sm-12" style="    text-align: center;color: #fff;font-size: 36px;margin-top: 60px;">
            TEMÀTIQUES
        </div>
        <div class="col-md-12 col-sm-12">
	        <?php 
		        $terms = get_terms( 'tematica', array(
					'hide_empty' => true,
					'orderby' => 'count',
					'order' => 'DESC'
				) );
				
				
			?>
		    <ul class="menu-large">

			<?php foreach($terms as $index => $term) : ?>     
				<?php if ($index > 36) continue; ?>           
                	<li><a href="/tematica/<?php echo $term->slug ?>" style="font-size: 14px;"><?php echo $term->name ?> (<?php echo $term->count ?>)</a></li>
                
                <?php if ($index % 9 == 0) : ?>
					</ul>
					</div>
					<div class="col-sm-3">
						<ul class="menu-large">
                <?php endif; ?>
                <?php endforeach ?>
               
            </ul>
					</div>
    </div>

<div class="col-md-12 col-sm-12"><hr class="linia hidden-xs"></div>