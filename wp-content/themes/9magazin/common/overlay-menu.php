<div id="navegation-site" class="overlay">
    <div class="container" style="position: relative;">             

        <button class="hamburger hamburger--squeeze js-hamburger is-active" type="button">
            <span class="hamburger-box">
              <span class="hamburger-inner"></span>
            </span>
        </button>
        
        <!-- logo -->
        <a class="navbar-brand" href="<?php echo site_url() ?>"><img src="<?php echo get_template_directory_uri() ?>/img/el9nou.cat-white.svg"></a>

        <!-- search -->
        <div class="search-btn"><i class="icon-lupa"></i></div>
        
        <hr style="border-color: #FFF; margin-top: 32px;">
        <div class="row">
            <!-- columna 1 -->
            <div class="col-md-8">
                
                <?PHP /* 
                <li><a href="<?php url_site('agenda') ?>">AGENDA</a></li>
                <li><a href="<?php url_site('9magazin') ?>">9MAGAZÍN</a></li>
                <li><a href="<?php url_site('fotogaleries') ?>">FOTOGALERIES</a></li>
                <li><a href="<?php url_site('el9tv') ?>">EL 9 TV</a></li>
                <li><a href="<?php url_site('el9fm') ?>">EL 9 FM</a></li>
                <li><a href="<?php url_site('9clics') ?>">9CLICS</a></li>                            
                <li><a href="<?php url_site('blogosfera') ?>">BLOGOSFERA</a></li>  
                */   ?>       

                <div class="row list-options">
                    <div class="col-md-3">
                        <ul class="menu">
                            <li>ACTUALITAT</li>
                            <li><a href="<?php url_site('el9nou') ?>/<?php echo edicio_actual() ?>/seccio/actualitat/politica">Política</a></li>
                            <li><a href="<?php url_site('el9nou') ?>/<?php echo edicio_actual() ?>/seccio/actualitat/societat">Societat</a></li>
                            <li><a href="<?php url_site('el9nou') ?>/<?php echo edicio_actual() ?>/seccio/actualitat/economia">Economia</a></li>                                          
                        </ul>
                    </div>
                    <div class="col-md-3">
                        <ul class="menu">
                            <li>ESPORTS</li>
                            <li><a href="<?php url_site('el9nou') ?>/<?php echo edicio_actual() ?>/seccio/esports/futbol">Futbol</a></li>
                            <li><a href="<?php url_site('el9nou') ?>/<?php echo edicio_actual() ?>/seccio/esports/hoquei-patins">Hoquei patins</a></li>
                            <li><a href="<?php url_site('el9nou') ?>/<?php echo edicio_actual() ?>/seccio/esports/motor">Motor</a></li>                            
                        </ul>
                    </div>
                    <div class="col-md-3">
                        <ul class="menu">
                            <li>CULTURA</li>
                            <li><a href="<?php url_site('el9nou') ?>/<?php echo edicio_actual() ?>/seccio/cultura-i-gent/arts-esceniques">Arts escèniques</a></li>
                            <li><a href="<?php url_site('el9nou') ?>/<?php echo edicio_actual() ?>/seccio/cultura-i-gent/cultura-popular">Cultura popular</a></li>            
                            <li><a href="<?php url_site('el9nou') ?>/<?php echo edicio_actual() ?>/seccio/cultura-i-gent/llibres">Llibres</a></li>            
                            <li><a href="<?php url_site('el9nou') ?>/<?php echo edicio_actual() ?>/seccio/cultura-i-gent/calaix">Calaix</a></li>                                            
                        </ul>
                    </div>
                    <div class="col-md-3">
                        <ul class="menu">
                            <li>SUPLEMENTS</li>
                            <li><a href="<?php url_site('9magazin') ?>">9magazín</a></li>
                            <li><a href="<?php url_site('agenda') ?>">Agenda</a></li>                            
                            <li><a href="<?php url_site('fotogaleries') ?>">Fotogaleria</a></li>                        
                            <li><a href="<?php url_site('blogosfera') ?>">Blogosfera</a></li>       
                        </ul>
                    </div>                    
                </div><!-- /row -->
                
                <hr>

                <div class="row list-options">
                    <div class="col-md-3">
                        <ul class="menu">
                            <li>SERVEIS</li>
                            <li><a href="<?php url_site('el9nou') ?>/serveis/el-temps">El temps</a></li>
                            <li><a href="<?php url_site('el9nou') ?>/serveis/farmacies">Farmàcies</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3">
                        <ul class="menu">
                            <li>DIGUES LA TEVA</li>
                            <li>Cartes al director, denúncies, queixes i comentaris, compartir fotografies i vídeos, altres...</li>
                        </ul>
                    </div>
                    <div class="col-md-3">
                        <ul class="menu">
                            <li>EL 9 NOU</li>
                            <li><a href="<?php url_site('el9nou') ?>/osona-ripolles/?canvi_edicio=osona-ripolles">Edició Osona i Ripollès</a></li>
                            <li><a href="<?php url_site('el9nou') ?>/valles-oriental/?canvi_edicio=valles-oriental">Edició Vallès Oriental</a></li>
                            <li><a href="<?php url_site('el9nou') ?>/osona-ripolles/edicio-paper/">Portada paper Osona</a></li>
                            <li><a href="<?php url_site('el9nou') ?>/valles-oriental/edicio-paper/">Portada paper Vallès</a></li>                            
                        </ul>
                    </div>
                    <div class="col-md-3">
                        <ul class="menu">
                            <li>SUBSCRIPCIONS</li>
                            <li>Tarifes subscripcions</li>
                            <li>Club del subscriptor</li>
                            <li>Butlletins</li>
                            <li>Contacte</li>
                        </ul>
                    </div>
                </div><!-- / row -->

                <hr>

                <div class="row list-options">
                    <div class="col-md-3">
                        <ul class="menu">
                            <li>PUBLICITAT</li>
                            <li>Tarifes publicitat</li>
                            <li>Publicacions oficials</li>
                            <li>Contacte</li>
                        </ul>
                    </div>
                    <div class="col-md-3">
                        <ul class="menu">
                            <li>PREMSA D’OSONA</li>
                            <li>Qui som</li>
                            <li>On som</li>
                            <li>Codi deontològic</li>
                            <li>Premis</li>
                            <li>Ideari</li>
                        </ul>
                    </div>
                    <div class="col-md-3">
                        <ul class="menu">
                            <li>ALTRES</li>
                            <li>Avís legal</li>
                            <li>Política de cookies</li>
                            <li>Contacte</li>
                        </ul>
                    </div>                    
                </div><!-- / row -->

            </div>
            <!-- columna 2 -->
            <div class="col-md-4 el-temps text-center">

                <h2>EL TEMPS | <span class="location">VIC</span>  <i class="fa fa-pencil" aria-hidden="true"></i></h2>
                
                <!-- el temps -->
                <div class="col-md-4 temps" id="temps-vic">                                     
                        <div class="imatge"></div>                        
                        <span class="dia">AVUI</span>      
                        <span class="temperatura"></span>                        
                </div>
                <div class="col-md-4 temps" id="temps-manlleu">                                     
                        <div class="imatge"></div>
                        <span class="dia">DIMECRES</span>
                        <span class="temperatura"></span>     
                </div>
               <div class="col-md-4 temps" id="temps-torello">
                        <div class="imatge"></div>
                        <span class="dia">DIJOUS</span>
                        <span class="temperatura"></span>      
                </div>
                
                <div class="row">
                  <?php
                    /* <img class="col-md-6" src="<?php echo get_template_directory_uri() ?>/img/9nou-subscriptor.png" style="margin-bottom: 20px;">
                    <img class="col-md-6" src="<?php echo get_template_directory_uri() ?>/img/9nou-paper.png" style="margin-bottom: 20px;"> 
                    <img class="col-md-6" src="<?php echo get_template_directory_uri() ?>/img/9nou-paper.png" style="margin-bottom: 20px;">
                    <img class="col-md-6" src="<?php echo get_template_directory_uri() ?>/img/9nou-subscriptor.png" style="margin-bottom: 20px;">
                    */ 
                    ?>
                </div>    
                
                <hr>
                               
                <h2 style="margin-bottom: 20px;">GRUP EL 9 NOU</h2>
                <div class="row">
                    <div class="col-md-6 logo-9fm">
                        <a href="<?php url_site('el9fm') ?>"><img src="<?php echo get_template_directory_uri() ?>/img/9FM-overlay.png"></a>
                        <span><a href="<?php url_site('el9fm') ?>">EN DIRECTE</a> | <a href="<?php url_site('el9fm') ?>/a-la-carta">A LA CARTA</a></span>
                    </div>
                    <div class="col-md-6 logo-9tv">
                        <img src="<?php echo get_template_directory_uri() ?>/img/9TV-overlay.png">
                        <span>EN DIRECTE | A LA CARTA</span>
                    </div>
                </div>
                 <div class="row">
                    <div class="col-md-6 logo-9clics">
                        <img src="<?php echo get_template_directory_uri() ?>/img/9clics-overlay.png">
                        <span>DIRECTORI | OFERTES</span>
                     </div>
                    <div class="col-md-6 logo-ctl">
                        <img src="<?php echo get_template_directory_uri() ?>/img/ctl-overlay.png">
                        <span>PROF. DE LA COMUNICACIÓ</span>
                    </div>                   
                </div>

                 <hr>

                 <h2 class="xarxes">XARXES SOCIALS</h2>
                 <ul class="compartir">
                    <li><a href="https://www.facebook.com/sharer/sharer.php?u=http://localhost:81/el9nou/wp/osona-ripolles/esports/no-soc-antimadridista-i-aixo-en-el-context-del-barca-no-es-normal/"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                    <li><a href="https://twitter.com/intent/tweet?url=http://localhost:81/el9nou/wp/osona-ripolles/esports/no-soc-antimadridista-i-aixo-en-el-context-del-barca-no-es-normal/"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                    <li><a href="https://plus.google.com/share?url=http://localhost:81/el9nou/wp/osona-ripolles/esports/no-soc-antimadridista-i-aixo-en-el-context-del-barca-no-es-normal/"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                    <li><a href="whatsapp://send?text=&lt;http://localhost:81/el9nou/wp/osona-ripolles/esports/no-soc-antimadridista-i-aixo-en-el-context-del-barca-no-es-normal/"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>                       
                    <li><a href="#"><i class="fa fa-link share-url" aria-hidden="true"></i></a></li>                       
                </ul>
            </div>

        </div>
        <div class="clear"></div>
        <hr style="border-color: #FFF;">
    </div>
</div>