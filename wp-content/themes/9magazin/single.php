<?php get_header('header-single') ?>

<section id="seccio">
    <div class="container">
        <div class="fitxa-post row"><?php 
                if (have_posts()) :
                    while (have_posts()) : the_post(); ?>
                        <div class="post col-md-12">
                            <h1 class="seccio">
                            <?php $terms = get_the_terms( $post->ID , 'tematica' );
                                    if ($terms) : 
                                        foreach ( $terms as $term ) {
                                            echo $term->name;
                                        } 
                            endif; ?> </h1>
                            <div class="row">
                                <div class="col-md-10 col-md-offset-1">
                                    <figure><?php the_post_thumbnail() ?></figure>
                                    <div class="row">
                                        <div class="col-md-3 hidden-xs"> 
                                            <div class="sponsored-inside">
                                                <span style="text-transform:uppercase"><?php echo the_tipus_patrocini() ?></span>
                                                <?php echo isset(get_field('patrocinat_per')->ID) ? get_the_post_thumbnail(get_field('patrocinat_per')->ID) : ''  ?>
                                            </div>                                            
                                        </div>  
                                        
                                        <div class="col-md-9">
                                            <div class="titol"  style="border-top: 1px solid #ddd">
                                                <h1 style="margin-top: 30px;"><?php the_title() ?></h1>
                                            </div>

                                            
                                            <div class="author"><?php 
                                                if (get_field('font')) :
                                                    if (get_field('enllaç')) : ?>
                                                       <i class="fa fa-user" aria-hidden="true"></i> <a href="<?php the_field('enllaç') ?>"><?php the_field('font') ?></a><?php
                                                    else : ?>
                                                        <i class="fa fa-user" aria-hidden="true"></i> <?php the_field('font');
                                                    endif;
                                                endif ?>
                                                <div class="data-post"><?php echo get_the_date() ?></div>
                                            </div>

                                            <?php the_content() ?>                                            
                                           
                                            <ul class="categories"><?php
                                                foreach (get_the_category() as $category) : ?>
                                                    <li><a href="<?php echo get_term_link($category) ?>"><i class="fa fa-tag" aria-hidden="true"></i> <?php echo $category->name ?></a></li>
                                                <?php endforeach ?>
                                            </ul>
                                            <ul class="tags"><?php 
                                                foreach (get_the_tags() as $tag) : ?>
                                                    <li><a href="<?php echo get_term_link($tag) ?>"><span>#</span><?php echo $tag->name ?> </a></li><?php 
                                                endforeach ?>
                                            </ul>

                                            <div class="post-bottom">
                                                 <div class="like inside">
                                                  <h5>M'agrada</h5><?php 
                                                    $meta = get_post_meta(get_the_ID(), 'likes', true) ?: 0 ?>
                                                    <likes-counter id="<?php echo get_the_ID() ?>" likes="<?php echo $meta ?>"></likes-counter>
                                                </div>
                                                <ul class="compartir-2" style="float: right">
                                                    <li>Compartir</li>
                                                    <li><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink() ?>"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                                    <li><a target="_blank" href="https://twitter.com/intent/tweet?text=<?php the_permalink() ?>"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                                    <li><a target="_blank" href="https://plus.google.com/share?url=<?php the_permalink() ?>"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                                                    <li class="whatsapp-single"><a id="share-whatsapp" href="whatsapp://send?text=<?php the_permalink() ?>" target="_blank"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
                                                    <li><a id="share-link" href="mailto:?subject=El%209%20Magazin&body=<?php the_permalink() ?>"><i class="fa fa-envelope" aria-hidden="true"></i></a></li>
                                                 </ul>
                                            </div>
                                            
                                        </div>

                                        <div class="col-md-3 visible-xs"> 
                                            <div class="sponsored-inside">
                                                <span style="text-transform:uppercase"><?php echo the_tipus_patrocini() ?></span>
                                                <?php echo isset(get_field('patrocinat_per')->ID) ? get_the_post_thumbnail(get_field('patrocinat_per')->ID) : ''  ?>
                                            </div>                                            
                                        </div> 
                                                    
                                        <div class="col-md-9 col-md-offset-3">

                                            <?php 
                                            $args = array(
                                                'posts_per_page'   => 3,
                                                'post_type'     => 'post',
                                                'meta_key'      => 'subtitol',
                                                'meta_value'    => get_field('subtitol'),
                                                'ignore_sticky_posts' => 1,
                                                'post__not_in' => array(get_the_ID()),
                                            );
                                            $the_query = new WP_Query( $args );
                                            if ($the_query->have_posts()) : ?>
                                                <div class="relacionats" style="margin-top: 20px;  margin-bottom: 60px;">
                                                    <h2>Altres articles sobre <?php the_field('subtitol') ?></h2>
                                                    <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
                                                        <a href="<?php the_permalink() ?>">
                                                            <div class="relacionat" style="border-bottom: 1px solid #ddd;overflow: hidden">
                                                                <div class="destacada pull-left" style="max-width: 125px;"><?php the_post_thumbnail() ?></div>
                                                                <div class="pull-left" style="padding: 15px;">
                                                                    <h4><?php the_title() ?></h4>
                                                                    Publicada el <?php echo get_the_date() ?>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    <?php endwhile ?>
                                                </div>
                                                <?php wp_reset_query(); ?>
                                            <?php endif ?>

                                            <div class="comentaris">
                                                <?php 
                                                // If comments are open or we have at least one comment, load up the comment template.
                                                if ( comments_open() || get_comments_number() ) :
                                                    comments_template();
                                                endif;
                                                ?>
                                            </div>
        
                                        </div>
                                                                                    
                                    </div>
                                </div>
                            </div>
                        </div><?php 
                    endwhile;
                endif; ?>
            </div>
        </div>
   </div>
</section>

<?php get_footer() ?>