<?php

$path = getcwd();
$folderName = str_replace(dirname($path), '', $path);
$directory = str_replace($folderName, '', $path);
include($directory . '/general-variables.php');

add_theme_support( 'post-thumbnails' ); 

function custom_rewrite_basic() {
 	add_rewrite_rule('(wp-content\/uploads\Z)', 'index.php?pagename=merda', 'top');
 	add_rewrite_rule('(wp-content\/uploads\Z)', 'https://s3-eu-west-1.amazonaws.com/9magazin/wp-content/uploads/$matches[1]', 'top');
 	add_rewrite_rule('(osona-ripolles\Z)', 'index.php?pagename=portada-osona-ripolles', 'top');
}
add_action('init', 'custom_rewrite_basic', 10, 0);

function es_patrocinat() {
	return get_field('tipus_patrocini');
}

function the_tipus_patrocini() {
	$patrocinis = array(
		'produit' => 'Produït',
		'recomanat' => 'Recomanat',
		'presentat' => 'Presentat'
	);
	echo isset($patrocinis[get_field('tipus_patrocini')]) ? $patrocinis[get_field('tipus_patrocini')] : '';
}

function create_posttype() {

	register_post_type( 'patrocinadors',
		array(
			'labels' => array(
				'name' => __( 'Patrocinadors' ),
				'singular_name' => __( 'Patrocinador' )
			),
			'supports' => array('title', 'thumbnail', 'editor'),
			'public' => true,
			'has_archive' => false,
		)
	);
}
add_action( 'init', 'create_posttype' );

add_action( 'wp_ajax_increase_count', 'increase_count' );
add_action( 'wp_ajax_nopriv_increase_count', 'increase_count' );
function increase_count() {
	$postId = $_POST['post_id'];
	$likes = get_post_meta($postId, 'likes', true);
	if ($likes == "") $likes = 0;
	update_post_meta($postId, 'likes', $likes + 1);
	echo $likes + 1;
	exit();
}

add_action( 'wp_ajax_decrease_count', 'decrease_count' );
add_action( 'wp_ajax_nopriv_decrease_count', 'decrease_count' );
function decrease_count() {
	$postId = $_POST['post_id'];
	$likes = get_post_meta($postId, 'likes', true);
	if ($likes == "") $likes = 0;
	update_post_meta($postId, 'likes', $likes - 1);
	echo $likes - 1;
	exit();
}

function registrar_tematica_taxonomy() {
	register_taxonomy('tematica', 'post', array(
			'label' 		=> __( 'Temàtica (Subtítiol)' ),
			'rewrite' 		=> array('slug' => 'tematica' ),
		)
	);
}
add_action( 'init', 'registrar_tematica_taxonomy' );


add_action( 'rest_api_init', function () {
	register_rest_route( 'api', 'osona-ripolles/ultimes', array(
		'methods' => 'GET',
		'callback' => 'ultimesEntrades',
	) );
} );


function ultimesEntrades(WP_REST_Request $request) {
	$total = $request->get_param( 'count' ) ?: 3;

	$posts = wp_get_recent_posts([
		'numberposts' => $total,
		'post_status' => 'publish'
	]);
	
	$output = [];
	foreach($posts as $post)
	{
		$object = [
			'nom' => $post['post_title'],
			'url' => get_permalink($post['ID']),
			'urlDestacada' => get_the_post_thumbnail_url($post['ID'], 'medium')
		];
		
		array_push($output, $object);
	
	}
	
	return $output;
}

add_action( 'rest_api_init', function () {
    register_rest_route( 'api', 'data', array(
        'methods' => 'GET',
        'callback' => 'perDia',
    ) );
} );

function perDia(WP_REST_Request $request) {
    $dia = $request->get_param( 'dia' ) ?: 3;

	$day = strtotime($dia);
	
	$args = [
	    'posts_per_page'  => 10,
	    'post_type'       => 'post',
	    'post_status'     => 'publish',
	    'date_query' => [
	        'year' => date('Y', $day) ,
	        'month' => date('m', $day),
	        'day' => date('d', $day)
		],
	]; 
	$posts = get_posts($args);

    $output = [];

    foreach($posts as $post)
    {
        $object = [
            'nom' => $post->post_title,
            'url' => get_permalink($post->ID),
            'urlDestacada' => get_the_post_thumbnail_url($post->ID)
        ];

        array_push($output, $object);

    }

    return $output;
}

add_action( 'rest_api_init', function () {
    register_rest_route( 'api', 'desde', array(
        'methods' => 'GET',
        'callback' => 'desde',
    ) );
} );

function desde(WP_REST_Request $request) {
    $dia = $request->get_param( 'dia' );

	$day = strtotime($dia);
	
	$args = [
	    'posts_per_page'  => 10,
	    'post_type'       => 'post',
	    'post_status'     => 'publish',
	    'date_query' => [
		    'after' => [
	        	'year' => date('Y', $day) ,
				'month' => date('m', $day),
				'day' => date('d', $day)
			],
		],
	]; 
	$posts = get_posts($args);

    $output = [];

    foreach($posts as $post)
    {
        $object = [
            'nom' => $post->post_title,
            'url' => get_permalink($post->ID),
            'urlDestacada' => get_the_post_thumbnail_url($post->ID),
            'entradeta' => wp_trim_words(get_post_field('post_content', $post->ID), 30),
        ];

        array_push($output, $object);

    }

    return $output;
}

function meks_disable_srcset( $sources ) {
    return false;
}
 
add_filter( 'wp_calculate_image_srcset', 'meks_disable_srcset' );