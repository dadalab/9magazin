 <!-- Share Overlay -->
 <div class="overlay-share">              
     <div class="overlayInner">
         <button class="hamburger hamburger--squeeze js-hamburger is-active" type="button">
              <span class="hamburger-box">
                  <span class="hamburger-inner"></span>
              </span>
         </button>
         <ul>
             <li>Compartir</li>
             <li><a id="share-facebook" href="#" target="_blank" data-share-url="https://www.facebook.com/sharer/sharer.php?u="><i class="fa fa-facebook" aria-hidden="true"></i> Facebook</a></li>
             <li><a id="share-twitter" href="#" target="_blank" data-share-url="https://twitter.com/intent/tweet?url="><i class="fa fa-twitter" aria-hidden="true"></i> Twitter</a></li>
             <li><a id="share-google" href="" data-share-url="https://plus.google.com/share?url="><i class="fa fa-google-plus" aria-hidden="true"></i> Google +</a></li>
             <li class="whatsapp"><a id="share-whatsapp" href="#" target="_blank" data-share-url="whatsapp://send?text="><i class="fa fa-whatsapp" aria-hidden="true"></i> Whatsapp</a></li>                       
             <li><a id="share-link" href="#" data-share-url="mailto:?subject=El%209%20Magazin&body="><i class="fa fa-envelope" aria-hidden="true"></i> Enviar a un amic</a></li>                       
             <!-- <li><a id="share-link" href="#"><i class="fa fa-link" aria-hidden="true"></i> Copy link</a></li> -->
         </ul>
     </div>
 </div>
 <!-- / Share Overlay -->