
    <div class="post featured col-md-12">
        <h1>
        <?php $terms = get_the_terms( $post->ID , 'tematica' );
            if ($terms) : 
                foreach ( $terms as $term ) {
                    echo '<a href="' . get_term_link($term->term_id, 'tematica') . '">' . $term->name . '</a>';
                } 
        endif; ?>        
        </h1>
        <a href="<?php the_permalink() ?>">
            <figure>
                <?php the_post_thumbnail() ?>
                <?php if (es_patrocinat()) : ?>
                    <div class="sponsored">
                        <?php echo get_the_post_thumbnail( get_field('patrocinat_per')->ID)  ?>
                        <span style="text-transform:uppercase"><?php echo the_tipus_patrocini() ?></span> 
                    </div>
                <?php endif ?>
            </figure>
        </a>
        <div class="row">
            <div class="titol col-md-6">                
                <a href="<?php the_permalink() ?>"><h1><?php the_title() ?></h1></a>     
                <div class="category hidden-sm hidden-xs"><?php 
                foreach(get_the_category() as $category) : ?>
                    <a href="<?php echo get_term_link($category) ?>"><i class="fa fa-tag" aria-hidden="true"></i> <?php echo $category->name ?></a><?php
                endforeach ?>
                </div>                                              
            </div>
            <div class="col-md-6">
                <?php the_excerpt() ?> 
                <div class="category visible-sm visible-xs"><?php 
                foreach(get_the_category() as $category) : ?>
                    <a href="<?php echo get_term_link($category) ?>"><i class="fa fa-tag" aria-hidden="true"></i> <?php echo $category->name ?></a><?php
                endforeach ?>
                </div>                  
            </div>
            <div class="like"><?php 
                $meta = get_post_meta(get_the_ID(), 'likes', true) ?: 0 ?>
                <likes-counter id="<?php echo get_the_ID() ?>" likes="<?php echo $meta ?>"></likes-counter>
            </div>

            <div class="compartir" data-url="<?php the_permalink() ?>" data-title="<?php the_title() ?>"><a href="#"><i class="icon-share"></i></a></div>
        </div>
        <hr>
    </div>
