<?php is_paged() ? get_header('header-single') : get_header() ?>

<?php if (! is_paged()) : ?>
 <div class="generalLoader">
        <div id="loader"></div>
    </div>
<?php endif ?>
	
	<?php if (! isset($_GET['s'])) : ?>
    <section id="<?php echo is_paged() ? 'seccio' : 'home' ?>">
        <div class="container">
            <div class="list-posts"><?php
                $destacats = new WP_Query(array('post__in' => get_option('sticky_posts'), 'posts_per_page' => 1, 'ignore_sticky_posts' => 1));
                if ($destacats->have_posts()) :
                    while ($destacats->have_posts()) : $destacats->the_post();
                        $postStickyId = get_the_ID();
                        if (is_sticky() && ! is_paged()) : ?>
                            <div class="row"><?php
                                get_template_part('templates/post', 'home-featured'); ?>
                            </div><?php 
                        endif;
                    endwhile;
                endif;
                wp_reset_query() ?>

                <div class="row grid"><?php 
                    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                    $articles = new WP_Query(
                        array(
                            'posts_per_page' => 6,
                            'paged' => $paged,
                            'ignore_sticky_posts' => 1,
                            'post__not_in' => array($postStickyId)
                        )
                    );
                    if ($articles->have_posts()) :
                        while ($articles->have_posts()) : $articles->the_post();
                            get_template_part('templates/post', 'home');
                        endwhile;
                    endif; ?>
				</div>
            </div>
             <div id="pagination-place"><?php
                wp_pagenavi();?>
            </div>
      	</div>
  	</section>
  	
  	<?php else : ?>
	  	<?php 
  		if (have_posts()) : ?>
  		
	  		<section id="seccio">
	  			<div class="container">
	  				<div class="list-posts"><?php
                        while (have_posts()) : the_post();
                            get_template_part('templates/post', 'home');
                        endwhile;
                    endif; ?>
	  				</div>
	  			</div>
	  		</section>
  	<?php endif ?>
<?php get_footer() ?>