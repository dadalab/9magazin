<!doctype html>
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title><?php bloginfo('name'); ?> | <?php is_front_page() ? bloginfo('description') : wp_title(''); ?></title>
    <?php /* <title><?php echo wp_title() ?> | 9Magazin</title> */ ?>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <link rel="shortcut icon" href="<?php echo site_url() ?>/favicon.ico" />
    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/normalize.css">
    <link rel="stylesheet" href="<?php echo url_site('el9nou'); ?>/wp-content/themes/el9nou/css/overlay-general.css">
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/main.css">
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/vendor/modernizr-2.8.3.min.js"></script>
    <script src="https://use.fontawesome.com/36eaf4504d.js"></script>
    <link rel="stylesheet" href="<?php url_site('el9nou') ?>/wp-content/themes/el9nou/css/footer.css">
    
    <?php wp_head() ?>
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/style.css?v=2.1">

    <?php if (is_single()) : ?>
        <meta property="og:url" content="<?php the_permalink() ?>"/>
        <meta property="og:image" content="<?php the_post_thumbnail_url('large') ?>"/>
        
        <?php $image = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'large');  ?>
        
        <meta property="og:image:width" content="<?php echo $image[1] ?>" />
		<meta property="og:image:height" content="<?php echo $image[2] ?>" />
        <meta property="og:title" content="<?php the_title() ?>" />
        <meta property="og:description" content="<?php echo get_the_excerpt() ?>" />
        
        <meta name="twitter:card" content="summary_large_image">
        <meta name="twitter:title" content="<?php the_title() ?>">
        <meta name="twitter:description" content="<?php echo get_the_excerpt() ?>">
        <meta name="twitter:image" content="<?php the_post_thumbnail_url('large') ?>">
    <?php endif ?>
    <script src="https://unpkg.com/vue@2.0.8/dist/vue.min.js"></script>
    <?php analytics_code() ?>
</head>
<body>
	<?php google_tag_manager() ?>

    <!--[if lt IE 8]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    
    <?php global_overlay_menu(__DIR__) ?>
    <?php //include('common/overlay-menu.php') ?>

    <?php include('common/topbar.php') ?>
      

    <header class="header" style="background: url('https://web.el9media.cat/9magazin/assets/2019/09/9magazin-tardor.jpg') no-repeat center center scroll;">            
		
		<?php /* https://s3-eu-west-1.amazonaws.com/9magazin/assets/2019/09/9magazin-tardor.jpg */ ?>
		
        <div class="header-wrapp container">
            <h1 class="suplement">Suplement</h1>
            
            <?php include('common/hamburger-button.php') ?>

            <a class="navbar-brand logo" href="<?php echo site_url() ?>">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/9magazin.svg">
            </a>
            
            <form class="search-field" style="display: none" method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">

	            	<input type="text" name="s">
					<small>Escriu el text a buscar i prem intro</small>
	            </form>


			<div class="search"><i class="icon-lupa"></i></div>
            <?php // include('common/search-button.php') ?>            
        </div>          
        <div class="down-arrow"><i class="fa fa-long-arrow-down" aria-hidden="true"></i></div>
    </header>

    <nav id="navigation">
        <div class="container">

            <?php include('common/hamburger-button.php') ?>
            
            <a class="navbar-brand logo" href="<?php echo site_url() ?>">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/9magazin.svg">
            </a>


			<form class="search-field" style="display: none" method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">

	            	<input type="text" name="s" style="    height: 40px;margin-top: 10px;">
					
	            </form>
			<div class="search"><i class="icon-lupa"></i></div>
            <?php // include('common/search-button.php') ?>
        </div>
    </nav>
