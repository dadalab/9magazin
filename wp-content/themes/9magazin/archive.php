<?php get_header('header-single') ?>

<section id="seccio">	
    <div class="container">
        <div class="list-posts row grid"><?php 
            if (have_posts()) :
                while (have_posts()) : the_post();
                    get_template_part('templates/post', 'home');
                endwhile;
            endif; ?>
        </div>
        <div id="pagination-place"><?php
            wp_pagenavi();?>
        </div>

   </div>
</section>

<?php get_footer() ?>