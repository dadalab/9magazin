
      <?php global_footer() ?>
      
      <?php include('templates/share-overlay.php'); ?>

           <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
           <script>window.jQuery || document.write('<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/vendor/jquery-1.12.0.min.js"><\/script>')</script>
           <script src="https://npmcdn.com/isotope-layout@3.0/dist/isotope.pkgd.min.js"></script>

           <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/vendor/jquery.easing.min.js"></script>
           
           <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/plugins.js"></script>
           <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.26/vue.min.js"></script>
           <script src="https://cdnjs.cloudflare.com/ajax/libs/vue-resource/0.9.3/vue-resource.min.js"></script>
           
           <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/main.js?v=4"></script>
		   
		   <script>
			   $('.search').click( function() {
				   
				    $('.logo').fadeOut(200).promise().done(function(){
						$('.search-field').fadeIn();
				   $('.search-field input').focus();
 					});
 

				   
			   });
			   
			</script>
		   
            <script type="text/javascript">
                $(window).load( function() {
                     $('.grid').isotope({
                          itemSelector: '.grid-item',
                          percentPosition: true,
                          masonry: {
                            columnWidth: '.grid-sizer'
                        }
                    })
                });
            </script>

        <?php wp_footer() ?>
    </body>
    </html>